package com.dxdg.pageobjects.product;

import com.dxdg.pageobjects.product.ProductDetail;
import com.dxdg.util.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Steven on 2015/5/24.
 */
public class Products {

    public ProductDetail clickProductDetailByLinkage(WebDriver driver, String s) {
        WebUtil.waitForElementVisible(driver, By.cssSelector("a[data-name='tab_product_info']"));

        WebUtil.click(driver, By.linkText(s));

        return PageFactory.initElements(driver, ProductDetail.class);
    }
}

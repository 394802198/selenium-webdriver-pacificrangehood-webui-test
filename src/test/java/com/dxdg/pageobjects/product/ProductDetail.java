package com.dxdg.pageobjects.product;

import com.dxdg.pageobjects.ProductFeatureChecklist;
import com.dxdg.util.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Steven on 2015/5/24.
 */
public class ProductDetail {

    public ProductDetail switchProductDetailByLinkage(WebDriver driver, String s) {
        WebUtil.waitForElementVisible(driver, By.linkText(s));

        WebUtil.click(driver, By.linkText(s));

        return PageFactory.initElements(driver, ProductDetail.class);
    }

    public ProductFeatureChecklist clickProductFeatureChecklist(WebDriver driver) {
        WebUtil.waitForElementVisible(driver, By.linkText("Rangehood Feature Checklist"));

        WebUtil.click(driver, By.linkText("Rangehood Feature Checklist"));

        return PageFactory.initElements(driver, ProductFeatureChecklist.class);
    }
}

package com.dxdg.pageobjects;

import com.dxdg.util.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Steven on 2015/5/24.
 */
public class AboutUs {

    public ContactUs clickContactUsLinkage(WebDriver driver) {
        WebUtil.waitForElementVisible(driver, By.linkText("Contact Us"));

        WebUtil.click(driver, By.linkText("Contact Us"));

        return PageFactory.initElements(driver, ContactUs.class);
    }
}

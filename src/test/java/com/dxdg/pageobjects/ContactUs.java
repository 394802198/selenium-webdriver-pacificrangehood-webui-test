package com.dxdg.pageobjects;

import com.dxdg.pageobjects.product.Products;
import com.dxdg.util.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Steven on 2015/5/24.
 */
public class ContactUs {

    public Products clickProductsLinkage(WebDriver driver) {
        WebUtil.waitForElementVisible(driver, By.linkText("Products"));

        WebUtil.click(driver, By.linkText("Products"));

        return PageFactory.initElements(driver, Products.class);
    }
}

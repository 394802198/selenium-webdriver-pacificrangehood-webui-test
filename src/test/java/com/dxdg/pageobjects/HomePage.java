package com.dxdg.pageobjects;

import com.dxdg.util.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Steven on 2015/5/24.
 */
public class HomePage {

    public AboutUs clickAboutUsLinkage(WebDriver driver) {
        WebUtil.waitForElementVisible(driver, By.linkText("About Us"));

        WebUtil.click(driver, By.linkText("About Us"));

        return PageFactory.initElements(driver, AboutUs.class);
    }

    public void clickHomePageLogo(WebDriver driver) {
        WebUtil.waitForElementVisible(driver, By.cssSelector("a[data-name='home_logo']"));

        WebUtil.click(driver, By.cssSelector("a[data-name='home_logo']"));
    }

    public boolean isHomePageExist(WebDriver driver) {
        WebUtil.waitForElementVisible(driver, By.cssSelector("a[href='product']"));

        return WebUtil.isElementDisplayed(driver, By.cssSelector("a[href='product']"));
    }
}

import com.dxdg.pageobjects.*;
import com.dxdg.pageobjects.product.ProductDetail;
import com.dxdg.pageobjects.product.Products;
import com.dxdg.util.WebUtil;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by Steven on 2015/5/24.
 */
public class PacificRangeHoodUITest {

    WebDriver driver;

    @Before
    public void setDriver(){
        String browserName = System.getenv("browser");
        if(browserName!=null && browserName.equalsIgnoreCase("Chrome")){
            driver = new ChromeDriver();
        } else {
            driver = new FirefoxDriver();
        }
    }

    @Test
    public void testAllPages(){
        // 1. Go Home Page
        HomePage homePage = WebUtil.goHomePage(driver);
        // 2. About Us
        AboutUs aboutUs = homePage.clickAboutUsLinkage(driver);
        // 3. Contact Us
        ContactUs contactUs = aboutUs.clickContactUsLinkage(driver);
        // 4. Products
        Products products = contactUs.clickProductsLinkage(driver);
        // 5. Click AutoClean AS-900
        ProductDetail productDetail = products.clickProductDetailByLinkage(driver, "AutoClean AS-900");
        // 6. Switch to MaxiFlow PR-936
        productDetail = productDetail.switchProductDetailByLinkage(driver, "MaxiFlow PR-936");
        // 7. Switch to MagiFlow PR-300
        productDetail = productDetail.switchProductDetailByLinkage(driver, "MagiFlow PR-300");
        // 8. Click Rangehood Feature Checklist
        ProductFeatureChecklist productFeatureChecklist = productDetail.clickProductFeatureChecklist(driver);
        // 9. Go Back Home Page
        homePage.clickHomePageLogo(driver);
        // 10. Verify If Is Home Page
        Assert.assertTrue("Home Page should be exist", homePage.isHomePageExist(driver));
    }


    @After
    public void tearDown(){
        driver.quit();
    }
}
